import React, { PureComponent } from 'react'

class CheckboxItem extends PureComponent {

  handleChange = () => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(this.props.value, !this.props.isChecked)
    }
  }

  render() {
    const { name, value, label, disabled, isChecked } = this.props

    return (
      <div className="checkbox-item">
        <label>
          <input
            type="checkbox"
            name={name}
            value={value}
            checked={isChecked}
            onChange={this.handleChange}
            disabled={disabled}
          />
          {label}
        </label>
      </div>
    )
  }
}

export default CheckboxItem