import React, { Component } from 'react';
import CheckboxList from './CheckboxList.component'
import { connect } from 'react-redux'
import moize from 'moize'
import {
  updateSelectedLessons,
  toggleSelectedLesson,
  getSelectedLessonsArray
} from './updateSelectedLessons.action'

// Add code in this file to create a component for the main view:
// You may want to connect component to store props & dispatch 

export class SelectedLessonsComponent extends Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.saveSelectedLessons();
  }

  render() {
    const { lessonsItems, selectedLessonsIds, toggleSelectedLesson, isLoading } = this.props;
    const isSubmitDisabled = isLoading || getSelectedLessonsArray(selectedLessonsIds).length === 0

    return (
      <div className={'selected-lessons' + (isLoading ? ' loading' : '')}>
        <h3>Lessons List:</h3>
        <form action="#@todo" method="post" onSubmit={this.handleSubmit}>
          <CheckboxList
            name="lessons"
            items={lessonsItems}
            selectedValues={selectedLessonsIds}
            disableAll={isLoading}
            onChange={toggleSelectedLesson}
          />
          <div className="submit-container">
            <input className="btn-submit" type="submit" disabled={isSubmitDisabled} />
          </div>
        </form>
      </div>
    )
  }
}

const getEnabledLessonsCheckboxData = moize((allLessons) => {
  // I assume a lesson might be also disabled and it should not be shown
  return allLessons
    .filter(lesson => lesson.status === 'enabled')
    .map(lesson => ({ label: lesson.name, value: lesson.id }))
});

export const mapStateToProps = state => {
  return {
    lessonsItems: getEnabledLessonsCheckboxData(state.allLessons),
    selectedLessonsIds: state.selectedLessons.selectedIds,
    isLoading: state.selectedLessons.isLoading
  }
}

const mapDispatchToProps = dispatch => {
  return {
    toggleSelectedLesson: (lessonId) => dispatch(toggleSelectedLesson(lessonId)),
    saveSelectedLessons: () => dispatch(updateSelectedLessons())
  }
}

const SelectedLessons = connect(
  mapStateToProps, 
  mapDispatchToProps
)(SelectedLessonsComponent)

export default SelectedLessons