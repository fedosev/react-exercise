/*
NOTE:
As I've told I never done unit test before for React components (just e2e tests),
so my method of testing might be naive. Anyway, the way I'm testing
SelectedLessons is by using the SelectedLessonsComponent
before is connected to Redux store. I'm passing only the props computed with
mapStateToProps to the component to test the output for the a specific state.
This method seems to be enough for these test specs. 
*/

import React from 'react';
import { SelectedLessonsComponent, mapStateToProps } from './SelectedLessons.component'
import CheckboxItem from './CheckboxItem.component'
import lessonItems from './all-lessons'

const state = {
  selectedLessons: {
    isLoading: false,
    selectedIds: {}
  },
  allLessons: lessonItems
}

const props = mapStateToProps(state)

const enabledLessons = lessonItems.filter(lesson => lesson.status === 'enabled')

describe('<SelectedLessons /> component', () => {
  
  it('should render', () => {
    const component = shallow(<SelectedLessonsComponent {...props} />)
    expect(component).toBeTruthy()
  })

  it('should display the correct name for the first item', () => {
    const component = mount(<SelectedLessonsComponent {...props} />)
    expect(component.find(CheckboxItem).first().text()).toContain(lessonItems[0].name)
  })

  it('should display the right number of items', () => {
    const component = mount(<SelectedLessonsComponent {...props} />)
    expect(component.find(CheckboxItem).length).toBe(enabledLessons.length)
  })

  it('should disable form if the data is being saved', () => {
    const props = mapStateToProps({
      selectedLessons: {
        isLoading: true,
        selectedIds: { 61: true }
      },
      allLessons: lessonItems
    })
    const component = mount(<SelectedLessonsComponent {...props} />)
    component.find('CheckboxItem').find('input').forEach(input => {
      expect(input.prop('disabled')).toBe(true)
    })
    expect(component.find('.btn-submit').prop('disabled')).toBe(true)
  })

  it('should disable the submit button if there are no lessons selected', () => {
    // The selected ids are only properties with value "true",
    // while the properties not defined or with value "false" are not selected.
    const selectedIdsArr = [
      { },
      { 61: false },
      { 61: false, 62: false, 76: false },
    ];
    selectedIdsArr.map(selectedIds => {
      const props = mapStateToProps({
        selectedLessons: {
          isLoading: false,
          selectedIds
        },
        allLessons: lessonItems
      })
      const component = mount(<SelectedLessonsComponent {...props} />)
      expect(component.find('.btn-submit').prop('disabled')).toBe(true)
    })
  })

  it('should enable the submit button if there are any lessons selected', () => {
    const selectedIdsArr = [
      { 61: true },
      { 61: true, 62: false },
      { 61: true, 62: true, 76: false }
    ];
    selectedIdsArr.map(selectedIds => {
      const props = mapStateToProps({
        selectedLessons: {
          isLoading: false,
          selectedIds
        },
        allLessons: lessonItems
      })
      const component = mount(<SelectedLessonsComponent {...props} />)
      expect(component.find('.btn-submit').prop('disabled')).toBe(false)
    })
  })

})