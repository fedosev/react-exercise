// create a component for a list of checkbox items here: 

import React, { PureComponent } from 'react'
import CheckboxItem from './CheckboxItem.component'

class CheckboxList extends PureComponent {

  handleChange = (value, isChecked) => {
    this.props.onChange(value, isChecked)
  }

  render() {
    return this.props.items.map(({ value, label }) => (
      <CheckboxItem
        key={value}
        name={this.props.name}
        value={value}
        label={label}
        isChecked={!!this.props.selectedValues[value]}
        disabled={this.props.disableAll}
        onChange={this.handleChange}
      />
    ))
  }
}

export default CheckboxList