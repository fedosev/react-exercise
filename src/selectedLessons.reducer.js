import { 
  UPDATE_SELECTED_LESSONS_REQUEST, 
  UPDATE_SELECTED_LESSONS_SUCCESS,
  TOGGLE_SELECTED_LESSON
} from './updateSelectedLessons.action'

// add code to make reducer work in this file. 

// selectedIds is and object instead of array for better performance
// on rendering the list (O(n) instead of O(n^2)) and simpler code
const initialState = {
  isLoading: false,
  selectedIds: {}
}

const selectedLessons = (state = initialState, action) => {
  switch (action.type) {
    case (UPDATE_SELECTED_LESSONS_REQUEST):
      return Object.assign({}, state, { isLoading: true })
    case (UPDATE_SELECTED_LESSONS_SUCCESS):
      return Object.assign({}, state, { isLoading: false })
    case (TOGGLE_SELECTED_LESSON):
      const lessonId = action.lessonId
      const selectedIds = Object.assign({}, state.selectedIds, { [lessonId]: !state.selectedIds[lessonId] })
      return Object.assign({}, state, { selectedIds })
    default:
      return state
  }
}

export default selectedLessons