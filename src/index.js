import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import SelectedLessons from './SelectedLessons.component'
import './styles.scss'
import reducer from './reducer'
import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose

const store = createStore(
  reducer, 
  {}, 
  composeEnhancers(applyMiddleware(thunk))
)

render(
  <Provider store={store}>
    <SelectedLessons />
  </Provider>,
  document.getElementById('root')
)