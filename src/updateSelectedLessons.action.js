
export const UPDATE_SELECTED_LESSONS_REQUEST = 'UPDATE_SELECTED_LESSONS_REQUEST'

export const UPDATE_SELECTED_LESSONS_SUCCESS = 'UPDATE_SELECTED_LESSONS_SUCCESS'

export const TOGGLE_SELECTED_LESSON = 'TOGGLE_SELECTED_LESSON'

export const updateSelectedLessonsRequest = () => {
  return {
    type: UPDATE_SELECTED_LESSONS_REQUEST
  }
}

export const updateSelectedLessonsSuccess = () => {
  return {
    type: UPDATE_SELECTED_LESSONS_SUCCESS
  }
}

export const toggleSelectedLesson = lessonId => {
  return {
    type: TOGGLE_SELECTED_LESSON,
    lessonId
  }
}

// fake an async request to API here:

export const updateSelectedLessons = () => {
  return async (dispatch, getState) => {
    dispatch(updateSelectedLessonsRequest())
    
    const { selectedLessons } = getState()
    const lessonIds = getSelectedLessonsArray(selectedLessons.selectedIds)

    try {
      const res = await fetch('https://admin.circusstreet.com/fake', {
        method: 'POST',
        mode: 'no-cors',
        body: JSON.stringify({ lessonIds })
      })
      // The following condition is always true just for the test purpose
      if (true || res.status === 200) {
        dispatch(updateSelectedLessonsSuccess())
      } else {
        throw new Error('Update selected lessons failed')
      }
    } catch (err) {
      // @todo: dispatch update error action
      console.warn(err)
    }
  }
}

// HELPERS

export function getSelectedLessonsArray(selectedIds = {}) {
  return Object.keys(selectedIds)
    .filter(id => !!selectedIds[id])
}
